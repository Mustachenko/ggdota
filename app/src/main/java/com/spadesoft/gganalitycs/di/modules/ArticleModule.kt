package com.spadesoft.gganalitycs.di.modules

import com.spadesoft.gganalitycs.adapters.ArticleAdapter
import com.spadesoft.gganalitycs.adapters.NewsListAdapter
import com.spadesoft.gganalitycs.model.ArticleModel
import dagger.Module
import dagger.Provides

@Module
class ArticleModule {
    @Provides
    fun articleItemsAdapterProvider(): NewsListAdapter {
        return NewsListAdapter(ArrayList())
    }

    @Provides
    fun articleAdapterProvider(): ArticleAdapter {
        return ArticleAdapter(ArrayList())
    }

    @Provides
    fun articleModelProvider(): ArticleModel {
        return ArticleModel()
    }
}