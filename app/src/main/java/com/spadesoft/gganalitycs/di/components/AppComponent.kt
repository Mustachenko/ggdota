package com.spadesoft.gganalitycs.di.components

import com.spadesoft.gganalitycs.App
import com.spadesoft.gganalitycs.adapters.ArticleAdapter
import com.spadesoft.gganalitycs.adapters.NewsListAdapter
import com.spadesoft.gganalitycs.di.modules.ArticleModule
import com.spadesoft.gganalitycs.di.modules.ViewModelModule
import com.spadesoft.gganalitycs.model.ArticleModel
import dagger.BindsInstance
import dagger.Component

@Component(modules = [ViewModelModule::class, ArticleModule::class])
interface AppComponent {

    val newsAdapter: NewsListAdapter
    val articleModel: ArticleModel
    val articleAdapter: ArticleAdapter

    fun inject(app: App)
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun withApplication(application: App): Builder

        fun build(): AppComponent
    }
}
