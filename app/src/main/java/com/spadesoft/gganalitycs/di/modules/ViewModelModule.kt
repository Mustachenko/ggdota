package com.spadesoft.gganalitycs.di.modules

import androidx.lifecycle.ViewModel
import com.spadesoft.gganalitycs.viewmodel.ArticleViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(ViewModel::class)
    internal abstract fun bindViewModel(viewModel: ArticleViewModel): ViewModel
}
