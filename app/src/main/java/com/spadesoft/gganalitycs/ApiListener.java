package com.spadesoft.gganalitycs;

public interface ApiListener {
    void onSuccess();

    void onError();
}
