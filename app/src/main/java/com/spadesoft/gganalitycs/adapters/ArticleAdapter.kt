package com.spadesoft.gganalitycs.adapters

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Canvas
import android.net.Uri
import android.os.Build
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import com.spadesoft.gganalitycs.R
import com.spadesoft.gganalitycs.firebase.imageRgx
import com.spadesoft.gganalitycs.firebase.videoRgx


class ArticleAdapter(private val articleParts: ArrayList<String>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val holder: RecyclerView.ViewHolder
        val inflater = LayoutInflater.from(parent.context)
        when (viewType) {
            1 -> {
                holder = ImageViewHolder(inflater.inflate(R.layout.article_image_part, parent, false))
            }
            2 -> {
                holder = VideoViewHolder(inflater.inflate(R.layout.article_video_part, parent, false))
            }
            else -> {
                holder = TextViewHolder(inflater.inflate(R.layout.article_text_part, parent, false))
            }
        }
        return holder
    }

    fun inflate(parts: ArrayList<String>) {
        articleParts.clear()
        articleParts.addAll(parts)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return articleParts.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        var part = articleParts[position]
        val root: View
        when (holder) {
            is ImageViewHolder -> {
                part = part.replace("<img:[", "").replace("]/>", "")
                root = holder.root
                holder.image = root.findViewById(R.id.image)
                Glide.with(root.context).load(part).optionalCenterCrop().into(holder.image)
            }
            is VideoViewHolder -> {
                root = holder.root
                holder.play = root.findViewById(R.id.play)
                part = part.replace("<video:[", "").replace("]/>", "")
                holder.image = root.findViewById(R.id.image)
                val videoSp = part.split("watch\\?(time_continue=\\d*\\&)?v=".toRegex())
                holder.play.setOnClickListener {
                    root.context.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(part)))
                }
                if (videoSp.size > 1)
                    Glide.with(root.context).asBitmap().load(Uri.parse("https://img.youtube.com/vi/${videoSp[1]}/hqdefault.jpg")).skipMemoryCache(
                        true
                    ).into(
                        object :
                            SimpleTarget<Bitmap>() {
                            override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                                val c = Canvas(resource)
                                c.drawColor(0x66000000)
                                holder.image.setImageBitmap(resource)
                            }
                        })
            }
            is TextViewHolder -> {
                root = holder.root
                holder.text = root.findViewById(R.id.text)
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    holder.text.text = Html.fromHtml(part, Html.FROM_HTML_MODE_LEGACY)
                } else holder.text.text = Html.fromHtml(part)
            }
        }
    }

    class TextViewHolder(val root: View) : RecyclerView.ViewHolder(root) {
        lateinit var text: TextView
    }

    open class ImageViewHolder(val root: View) : RecyclerView.ViewHolder(root) {
        lateinit var image: ImageView
    }

    class VideoViewHolder(val root: View) : RecyclerView.ViewHolder(root) {
        lateinit var image: ImageView
        lateinit var play: ImageButton
    }

    override fun getItemViewType(position: Int): Int {
        return when {
            articleParts[position].matches(imageRgx) -> 1
            articleParts[position].matches(videoRgx) -> 2
            else -> 3
        }
    }
}