package com.spadesoft.gganalitycs.adapters

import android.annotation.SuppressLint
import android.os.Build
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.spadesoft.gganalitycs.R
import com.spadesoft.gganalitycs.callbacks.NewsItemDiffUtilCallack
import com.spadesoft.gganalitycs.pojo.Article
import uk.co.deanwild.flowtextview.FlowTextView

class NewsListAdapter(val articles: ArrayList<Article>) : RecyclerView.Adapter<NewsListAdapter.NewsHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsHolder {
        return NewsHolder(LayoutInflater.from(parent.context).inflate(R.layout.article_item_layout, parent, false))
    }

    lateinit var openArticleOnClick: OpenArticleOnClick

    interface OpenArticleOnClick {
        fun open(pos: Int)
    }

    fun loadInItems(newArticles: ArrayList<Article>) {
        val result = DiffUtil.calculateDiff(NewsItemDiffUtilCallack(articles, newArticles), true)
        articles.clear()
        articles.addAll(newArticles)
        result.dispatchUpdatesTo(this)
    }

    fun clear() {
        articles.clear()
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return articles.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: NewsHolder, position: Int) {
        val root = holder.root
        val article = articles[position]

        holder.content = root.findViewById(R.id.preview_content)
        holder.preview = root.findViewById(R.id.article_preview)
        holder.header = root.findViewById(R.id.item_header)
        holder.time = root.findViewById(R.id.publication_time)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            holder.content.text = Html.fromHtml(article.content[0], Html.FROM_HTML_MODE_COMPACT)
        } else holder.content.text = Html.fromHtml(article.content[0])


        root.setOnClickListener {
            openArticleOnClick.open(position)
        }


        holder.time.text = article.pubTime

        Glide.with(root.context).load(article.headerImage.replace("<img:[", "").replace("]/>", ""))
            .into(holder.preview)

        holder.header.textColor = root.resources.getColor(android.R.color.white)
        holder.header.setTextSize(root.resources.getDimension(R.dimen.header_text_size))
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            holder.header.text = Html.fromHtml(article.headerText, Html.FROM_HTML_MODE_COMPACT)
        } else holder.header.text = Html.fromHtml(article.headerText)
    }


    class NewsHolder(val root: View) : RecyclerView.ViewHolder(root) {
        lateinit var preview: ImageView
        lateinit var time: TextView
        lateinit var header: FlowTextView
        lateinit var content: TextView
    }
}