package com.spadesoft.gganalitycs;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiService {
    @GET("json?token=8443b852df8803")
    Call<IPGeo> getIPLocation();
}
