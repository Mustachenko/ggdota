package com.spadesoft.gganalitycs.viewmodel

import android.app.Application
import android.os.Handler
import androidx.lifecycle.AndroidViewModel
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.spadesoft.gganalitycs.App
import com.spadesoft.gganalitycs.adapters.ArticleAdapter
import com.spadesoft.gganalitycs.adapters.NewsListAdapter
import com.spadesoft.gganalitycs.callbacks.OnCompleteDataRetrieve
import com.spadesoft.gganalitycs.pojo.Article
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ArticleViewModel @Inject constructor(application: Application) : AndroidViewModel(application) {
    private val model = App.appComponent.articleModel
    var listAdapter: NewsListAdapter = App.appComponent.newsAdapter

    val articleAdapter: ArticleAdapter by lazy { App.appComponent.articleAdapter }

    fun inflateNews(refreshLayout: SwipeRefreshLayout) {
        Handler().postDelayed({
            if (refreshLayout.isRefreshing)
                refreshLayout.isRefreshing = false
        }, 3000)
        model.getArticles(object : OnCompleteDataRetrieve {
            override fun retrieve(items: List<Any>) {
                val items = items as ArrayList<Article>
                val sortedItems = items.sortedWith(compareByDescending<Article> {
                    val sdf = SimpleDateFormat("hh:mm dd.MM")
                    val date = sdf.parse(it.pubTime)
                    date
                }.thenBy { it.headerText })
                listAdapter.loadInItems(sortedItems.toMutableList() as ArrayList<Article>)
                refreshLayout.isRefreshing = false
            }
        })
    }

    fun inflateArticle(article: Article) {
           articleAdapter.inflate(article.content)
    }

}