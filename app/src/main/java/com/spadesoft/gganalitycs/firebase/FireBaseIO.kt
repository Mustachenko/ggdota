package com.spadesoft.gganalitycs.firebase

import com.spadesoft.gganalitycs.pojo.Article
import com.google.firebase.storage.FirebaseStorage
import io.reactivex.Observable


const val text = "([А-Яа-я]|\\w|\\d|\\W)+?"
val headerRgx = """<header\s+time:"$text"\s*>$text<header\/>""".toRegex()
val timeRgx = """time:"$text"""".toRegex()
val imageRgx = """<img:\[$text\]\/>""".toRegex()
val videoRgx = """<video:\[$text\]\/>""".toRegex()


fun getArticlesList(): Observable<String> {
    return Observable.create { emitter ->
        val ref = FirebaseStorage.getInstance().reference.child("dota_articles").child("articles_list.txt")
        ref.getStream { p0, stream ->
            val br = stream.bufferedReader()
            br.forEachLine {
                if (it.isNotBlank())
                    emitter.onNext(it)
            }
            stream.close()
            emitter.onComplete()
        }
        ref.metadata.addOnFailureListener { emitter.onComplete() }
    }
}

fun getArticles(articles: List<String>): Observable<Article> {
    val parsed = ArrayList<Int>()
    return Observable.create { emitter ->
        for (it in 0 until articles.size) {
            val refPath = articles[it].trim()
            val children = refPath.replace("gs://gg-play-9f3e8.appspot.com/", "").split("/")
            var ref = FirebaseStorage.getInstance().reference
            children.forEach {
                if (it.isNotBlank()) ref = ref.child(it.replace("\ufeff", ""))
            }

            ref.metadata.addOnSuccessListener { _ ->
                ref.getStream { _, stream ->
                    val br = stream.bufferedReader()
                    var content = br.readText()

                    var headerPart = headerRgx.find(content, 0)?.value

                    headerPart?.let { it1 -> content = content.replace(it1, "") }

                    var headerImage = ""
                    val headerTime = headerPart?.let { it1 ->
                        timeRgx.find(it1, 0)
                            ?.value?.replace("time:\"", "")?.replace(""""""", "")
                    }

                    headerPart =
                        headerPart?.replace("<header\\s+time:\"$text\"\\s*>".toRegex(), "")?.replace("<header/>", "")

                    val headerText = headerPart?.replace(imageRgx, "")?.replace(timeRgx, "")
                    if (!headerPart.isNullOrBlank()) {
                        val t = imageRgx.find(headerPart, 0)?.value
                        if (t != null) headerImage = t
                    }

                    val contentParts = ArrayList<String>()

                    val texts = content.split("($imageRgx)|($videoRgx)".toRegex(), 0)

                    texts.forEach { text ->
                        contentParts.add(text)

                        videoRgx.find(content)?.value?.let { it1 ->
                            val txt = StringBuffer()
                            val chars = text.trim().toCharArray()
                            for (i in 0 until chars.size) {
                                val bul =
                                    chars[i] == '/' || chars[i] == '*' || chars[i] == '$' || chars[i] == '[' || chars[i] == ']' || chars[i] == '^' ||
                                            chars[i] == '?' || chars[i] == ':' || chars[i] == '!' || chars[i] == '=' || chars[i] == '+'
                                            || chars[i] == '}' || chars[i] == '{' || chars[i] == '|' || chars[i] == ','
                                            || chars[i] == '(' || chars[i] == ')' || chars[i] == '-'
                                if (i != 0)
                                    if (chars[i - 1] != '\\')
                                        if (bul)
                                            txt.append("\\")
                                txt.append(chars[i])

                            }

                            val found = ("$txt\\s*\\W*$videoRgx").toRegex().find(content, 0)
                            if (found != null) {
                                contentParts.add(it1.trim())
                                content = content.replace(it1, "")
                            }
                        }
                        imageRgx.find(content)?.value?.let { it1 ->
                            val chars = text.toMutableList()
                            val txt = StringBuilder()
                            for (i in 0 until chars.size) {
                                val bul =
                                    chars[i] == '/' || chars[i] == '*' || chars[i] == '$' || chars[i] == '[' || chars[i] == ']' || chars[i] == '^' ||
                                            chars[i] == '?' || chars[i] == ':' || chars[i] == '!' || chars[i] == '=' || chars[i] == '+'
                                            || chars[i] == '}' || chars[i] == '{' || chars[i] == '|' || chars[i] == ','
                                            || chars[i] == '(' || chars[i] == ')' || chars[i] == '-'
                                if (i != 0)
                                    if (chars[i - 1] != '\\')
                                        if (bul)
                                            txt.append("\\")
                                txt.append(chars[i])
                            }
                            val found = "$txt\\s*\\W*$imageRgx".toRegex().find(content, 0)
                            if (found != null) {
                                contentParts.add(it1.trim())
                                content = content.replace(it1, "")
                            }
                        }
                    }
                    if (headerText != null && headerTime != null) {
                        val article = Article(headerText.trim(), headerImage.trim(), contentParts, headerTime.trim())
                        emitter.onNext(article)
                    }
                    parsed.add(it)
                    stream.close()
                    if (parsed.size == articles.size) {
                        emitter.onComplete()
                    }
                }
            }.addOnFailureListener { _ ->
                parsed.add(it)
                if (parsed.size == articles.size) {
                    emitter.onComplete()
                }
            }
        }
    }

}



