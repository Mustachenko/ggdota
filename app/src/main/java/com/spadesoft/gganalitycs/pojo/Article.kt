package com.spadesoft.gganalitycs.pojo

const val POSITION = "POSITION"

data class Article(
    var headerText: String,
    var headerImage: String,
    var content: ArrayList<String>,
    var pubTime: String
)