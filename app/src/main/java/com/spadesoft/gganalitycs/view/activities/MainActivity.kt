package com.spadesoft.gganalitycs.view.activities

import android.content.pm.ActivityInfo
import android.os.Bundle
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProviders
import com.spadesoft.gganalitycs.R
import com.spadesoft.gganalitycs.factories.DaggerViewModelFactory
import com.spadesoft.gganalitycs.view.fragments.MainPageFragment
import com.spadesoft.gganalitycs.viewmodel.ArticleViewModel
import javax.inject.Inject


@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class MainActivity : FragmentActivity() {

    private var viewModelFactory: DaggerViewModelFactory? = null
    private lateinit var viewModel: ArticleViewModel

    @Inject
    fun initViewModelFactory(factory: DaggerViewModelFactory) {
        viewModelFactory = factory
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        setContentView(R.layout.activity_main)

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(ArticleViewModel::class.java)
        supportFragmentManager.beginTransaction().add(R.id.container, MainPageFragment()).commit()
    }
}

