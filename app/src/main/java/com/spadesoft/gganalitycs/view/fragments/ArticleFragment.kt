package com.spadesoft.gganalitycs.view.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.spadesoft.gganalitycs.R
import com.spadesoft.gganalitycs.factories.DaggerViewModelFactory
import com.spadesoft.gganalitycs.pojo.POSITION
import com.spadesoft.gganalitycs.viewmodel.ArticleViewModel
import com.r0adkll.slidr.Slidr
import com.r0adkll.slidr.model.SlidrConfig
import com.r0adkll.slidr.model.SlidrInterface
import com.r0adkll.slidr.model.SlidrListener
import com.r0adkll.slidr.model.SlidrPosition
import javax.inject.Inject

class ArticleFragment : Fragment() {

    var slidrInterface: SlidrInterface? = null
    private var daggerViewModelFactory: DaggerViewModelFactory? = null
    private lateinit var articleViewModel: ArticleViewModel

    @Inject
    fun initViewModelFactory(factory: DaggerViewModelFactory) {
        daggerViewModelFactory = factory
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        articleViewModel = ViewModelProviders.of(activity!!, daggerViewModelFactory).get(ArticleViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_article, container, false)
        val pos = arguments?.getInt(POSITION)
        if (pos != null) {
            val article = articleViewModel.listAdapter.articles[pos]
            val header = root.findViewById<TextView>(R.id.header)
            val articleContent = root.findViewById<RecyclerView>(R.id.article)
            articleContent.layoutManager = LinearLayoutManager(context)
            articleContent.adapter = articleViewModel.articleAdapter
            article.let { articleViewModel.inflateArticle(it) }
            val headerImage = root.findViewById<ImageView>(R.id.headerImage)
            root.context.let {
                Glide.with(it).load(article.headerImage.replace("<img:[", "").replace("]/>", "")).into(headerImage)
            }
            header.text = article.headerText
        }
        return root
    }

    override fun onResume() {
        super.onResume()
        val view = view?.findViewById<View>(R.id.content_container)
        if (slidrInterface == null) {
            view?.let {
                Slidr.replace(
                    it, SlidrConfig.Builder()
                        .sensitivity(0.6f)
                        .listener(object : SlidrListener {
                            override fun onSlideClosed() {

                            }

                            override fun onSlideStateChanged(state: Int) {}

                            override fun onSlideChange(percent: Float) {
                                if (percent < 0.7f) {
                                }
                            }

                            override fun onSlideOpened() {

                            }

                        }).position(SlidrPosition.LEFT).build()
                )
            }
        }

    }
}

