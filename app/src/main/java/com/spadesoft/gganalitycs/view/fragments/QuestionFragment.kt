package com.spadesoft.gganalitycs.view.fragments


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import com.spadesoft.gganalitycs.R

class QuestionFragment : Fragment(), View.OnClickListener {
    private lateinit var next: NextListener

    companion object {
        fun getInstance(next: NextListener): QuestionFragment {
            val f = QuestionFragment()
            f.next = next
            return f
        }
    }

    interface NextListener {
        fun onNext()
    }

    private lateinit var goOnButton: Button

    override fun onClick(v: View?) {
        next.onNext()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val root = inflater.inflate(R.layout.fragment_question, container, false)
        goOnButton = root.findViewById(R.id.go_on_button)
        goOnButton.setOnClickListener(this)
        return root
    }


}
