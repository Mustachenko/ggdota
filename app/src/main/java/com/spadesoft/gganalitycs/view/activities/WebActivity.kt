package com.spadesoft.gganalitycs.view.activities

import android.content.pm.ActivityInfo
import android.net.Uri
import android.net.http.SslError
import android.os.Build
import android.os.Bundle
import android.os.Message
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.webkit.*
import android.widget.FrameLayout
import androidx.appcompat.app.AppCompatActivity
import com.spadesoft.gganalitycs.R


private const val target_url = "http://game.winrewardofficial.com/c/9d8a3f10a5f3c354"
private const val target_url_prefix = "http://game.winrewardofficial.com"


class WebActivity : AppCompatActivity() {
    internal var webView: WebView? = null
    private var mWebviewPop: WebView? = null
    private var mContainer: FrameLayout? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        setContentView(R.layout.activity_web)
        webView = findViewById(R.id.webView)
        mContainer = findViewById(R.id.frame_layout)
        webView!!.webViewClient = UriWebViewClient()
        webView!!.webChromeClient = UriChromeClient()
        webView!!.settings.javaScriptEnabled = true
        webView!!.settings.setSupportMultipleWindows(true)
        webView!!.settings.javaScriptCanOpenWindowsAutomatically = true
        webView!!.settings.loadWithOverviewMode = true
        webView!!.settings.useWideViewPort = true

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            CookieManager.getInstance().setAcceptThirdPartyCookies(webView, true)
        } else {
            CookieManager.getInstance().acceptCookie()
        }
        webView!!.settings.setSupportZoom(true)
        webView!!.settings.builtInZoomControls = true
        webView!!.settings.displayZoomControls = false
        webView!!.settings.pluginState = WebSettings.PluginState.ON

        webView!!.scrollBarStyle = WebView.SCROLLBARS_OUTSIDE_OVERLAY
        webView!!.isScrollbarFadingEnabled = false
        webView!!.settings.domStorageEnabled = true
        if (savedInstanceState == null) {
            webView!!.loadUrl(target_url)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        webView!!.saveState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        webView!!.restoreState(savedInstanceState)
    }

    override fun onDestroy() {
        if (webView != null)
            webView!!.destroy()

        if (mWebviewPop != null)
            mWebviewPop!!.destroy()
        super.onDestroy()
    }

    private inner class UriWebViewClient : WebViewClient() {
        override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
            val host = Uri.parse(url).host
            Log.d("tag", "url " + url + "host " + host)
            //Log.d("shouldOverrideUrlLoading", url);
            if (host == target_url_prefix) {
                // This is my web site, so do not override; let my WebView load
                // the page
                if (mWebviewPop != null) {
                    mWebviewPop!!.visibility = View.GONE
                    mContainer!!.removeView(mWebviewPop)
                    mWebviewPop = null
                }
                return false
            } else {
                if (mWebviewPop != null) {
                    //                    mWebviewPop.loadUrl(url);
                    //                    Log.d("tag", "popView loadUrl " + url);
                }
            }

            return false
        }

        override fun onReceivedSslError(
            view: WebView, handler: SslErrorHandler,
            error: SslError
        ) {
            Log.d("tag", "onReceivedSslError")
        }
    }

    internal inner class UriChromeClient : WebChromeClient() {

        override fun onCreateWindow(
            view: WebView, isDialog: Boolean,
            isUserGesture: Boolean, resultMsg: Message
        ): Boolean {
            mWebviewPop = WebView(this@WebActivity)
            mWebviewPop!!.isVerticalScrollBarEnabled = false
            mWebviewPop!!.isHorizontalScrollBarEnabled = false
            mWebviewPop!!.webViewClient = UriWebViewClient()
            mWebviewPop!!.webChromeClient = UriChromeClient()
            mWebviewPop!!.settings.javaScriptEnabled = true
            mWebviewPop!!.settings.pluginState = WebSettings.PluginState.ON
            mWebviewPop!!.settings.setSupportMultipleWindows(true)
            mWebviewPop!!.settings.javaScriptCanOpenWindowsAutomatically = true
            mWebviewPop!!.settings.domStorageEnabled = true
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                CookieManager.getInstance().setAcceptThirdPartyCookies(webView, true)
            } else {
                CookieManager.getInstance().acceptCookie()
            }
            mWebviewPop!!.layoutParams = FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )
            mContainer!!.addView(mWebviewPop)
            val transport = resultMsg.obj as WebView.WebViewTransport
            transport.webView = mWebviewPop
            resultMsg.sendToTarget()

            return true
        }

        override fun onCloseWindow(window: WebView) {
            super.onCloseWindow(window)
            mContainer!!.removeView(window)

        }
    }

    override fun onBackPressed() {
        if(mWebviewPop?.canGoBack() == true) {
            mWebviewPop?.goBack()
        }
        if (webView!!.canGoBack()) {
            webView!!.goBack()
        } else {
            super.onBackPressed()
        }
    }


}
