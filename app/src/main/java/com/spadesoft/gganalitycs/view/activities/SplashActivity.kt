package com.spadesoft.gganalitycs.view.activities

import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.os.Handler
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentManager
import com.bumptech.glide.Glide
import com.spadesoft.gganalitycs.ApiListener
import com.spadesoft.gganalitycs.ApiService
import com.spadesoft.gganalitycs.IPGeo
import com.spadesoft.gganalitycs.R
import com.spadesoft.gganalitycs.view.fragments.NoConnectionFragment
import com.spadesoft.gganalitycs.view.fragments.QuestionFragment
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

const val PREF_FILE_NAME = "PrefFile"

class SplashActivity : AppCompatActivity(), ApiListener, NoConnectionFragment.RefreshListener {
    private lateinit var loadingImage: ImageView
    private lateinit var sPref: SharedPreferences
    private lateinit var fm: FragmentManager
    private var activityNotOpened = true

    private var isResponseSuccessful: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
       super.onCreate(savedInstanceState)

            requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        isResponseSuccessful = false
        setContentView(R.layout.activity_splash)

        loadingImage = findViewById(R.id.loading_progress)

        setModeLoading()
        fm = supportFragmentManager

        sPref = getSharedPreferences(PREF_FILE_NAME, MODE_PRIVATE)
        val savedText = sPref.getString("launch", "")
        try {
            if (savedText == "web") {
                openActivity(savedText)
            } else {
                Handler().postDelayed({
                    showMainPage()
                }, 2000)
            }
        } catch (e: NullPointerException) {
            showMainPage()
        }

    }

    private fun showMainPage() {
        val ft = fm.beginTransaction()
        ft.replace(R.id.container, QuestionFragment.getInstance(object : QuestionFragment.NextListener {
            override fun onNext() {
                getCountryFromApi()
            }
        }))
        ft.setCustomAnimations(
            android.R.anim.fade_in,
            android.R.anim.fade_out,
            android.R.anim.fade_in,
            android.R.anim.fade_out
        )
        ft.commitAllowingStateLoss()
    }

    private fun openActivity(mode: String) {
        if (activityNotOpened)
            if (mode == "web") {
                startActivity(Intent(this, WebActivity::class.java))
            } else {
                startActivity(Intent(this, MainActivity::class.java))
            }
        activityNotOpened = false
        finish()
    }

    private fun getCountryFromApi() {
        val retrofit = Retrofit.Builder()
            .baseUrl("https://ipinfo.io/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val service = retrofit.create<ApiService>(ApiService::class.java)
        service.ipLocation.enqueue(object : Callback<IPGeo> {
            override fun onResponse(call: Call<IPGeo>, response: Response<IPGeo>) {
                val country = response.body()!!.country
                try {
                    val sPref = getSharedPreferences(PREF_FILE_NAME, MODE_PRIVATE)
                    val ed = sPref.edit()
                    if (country == "RU" || country == "KZ"
                        || country == "BE" || country == "AZ"
                        || country == "AM" || country == "KG"
                        || country == "MD" || country == "UZ"
                        || country == "TJ" || country == "BR"
                    ) {
                        ed.putString("launch", "web")
                        ed.commit()
                    }
                } catch (e: NullPointerException) {
                    e.printStackTrace()
                }

                val savedText = sPref.getString("launch", "")
                openActivity(savedText!!)
            }

            override fun onFailure(call: Call<IPGeo>, t: Throwable) {
                val ft = fm.beginTransaction()
                ft.replace(R.id.container, NoConnectionFragment.getInstance(this@SplashActivity))
                ft.setCustomAnimations(
                    android.R.anim.fade_in,
                    android.R.anim.fade_out,
                    android.R.anim.fade_in,
                    android.R.anim.fade_out
                )
                ft.commitAllowingStateLoss()
            }
        })
    }

    override fun onSuccess() {
        isResponseSuccessful = true
    }

    override fun onError() {
        isResponseSuccessful = false
    }

    override fun onRefresh() {
        getCountryFromApi()
    }


    private fun setModeLoading() {
        loadingImage.layoutParams.height = resources.getDimensionPixelSize(R.dimen.generic_logo_size)
        loadingImage.requestLayout()
        Glide.with(this).asGif().load(R.drawable.loading).into(loadingImage)
    }
}
