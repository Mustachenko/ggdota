package com.spadesoft.gganalitycs.view.fragments


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.spadesoft.gganalitycs.R
import com.spadesoft.gganalitycs.adapters.NewsListAdapter
import com.spadesoft.gganalitycs.factories.DaggerViewModelFactory
import com.spadesoft.gganalitycs.pojo.POSITION
import com.spadesoft.gganalitycs.viewmodel.ArticleViewModel
import javax.inject.Inject


class MainPageFragment : Fragment() {
    private lateinit var articleViewModel: ArticleViewModel
    private var daggerViewModelFactory: DaggerViewModelFactory? = null
    private lateinit var newsRecyclerView: RecyclerView
    private lateinit var refresh: SwipeRefreshLayout
    @Inject
    fun initViewModelFactory(daggerViewModelFactory: DaggerViewModelFactory) {
        this.daggerViewModelFactory = daggerViewModelFactory
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        articleViewModel = ViewModelProviders.of(activity!!, daggerViewModelFactory).get(ArticleViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_main_page, container, false)
        newsRecyclerView = root.findViewById(R.id.news)
        newsRecyclerView.layoutAnimation =
            AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_waterfall)
        refresh = root.findViewById(R.id.refresh)
        refresh.setColorSchemeColors(resources.getColor(R.color.colorThird))
        refresh.setProgressBackgroundColorSchemeColor(resources.getColor(R.color.colorPrimary))
        refresh.setOnRefreshListener {
            articleViewModel.inflateNews(refresh)
        }
        articleViewModel.listAdapter.openArticleOnClick = object : NewsListAdapter.OpenArticleOnClick {
            override fun open(pos: Int) {
                val fragment = ArticleFragment()
                val args = Bundle()
                args.putInt(POSITION, pos)
                fragment.arguments = args
                activity?.supportFragmentManager?.beginTransaction()?.setCustomAnimations(
                    android.R.anim.slide_in_left, android.R.anim.slide_out_right,
                    android.R.anim.slide_in_left,
                    android.R.anim.slide_out_right
                )?.add(R.id.container, fragment)?.addToBackStack("article")?.commit()
            }
        }
        newsRecyclerView.layoutManager = LinearLayoutManager(context)
        newsRecyclerView.adapter = articleViewModel.listAdapter
        return root
    }

    override fun onResume() {
        super.onResume()
        refresh.isRefreshing = true
        articleViewModel.inflateNews(refresh)
    }
}
