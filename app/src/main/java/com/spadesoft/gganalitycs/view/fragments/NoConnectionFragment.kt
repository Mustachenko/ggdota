package com.spadesoft.gganalitycs.view.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import com.spadesoft.gganalitycs.R

@SuppressLint("ValidFragment")
class NoConnectionFragment(var listener: RefreshListener) : Fragment(), View.OnClickListener {
    private lateinit var refreshBtn: ImageView


    interface RefreshListener {
        fun onRefresh()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = layoutInflater.inflate(R.layout.fragment_no_connection, container, false)
        refreshBtn = v.findViewById(R.id.refresh_btn)
        refreshBtn.setOnClickListener(this)
        return v
    }

    override fun onClick(view: View) {
        listener.onRefresh()
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("tag", "OnDestroy NoConnectionFragment")
    }

    companion object {

        var fragment: NoConnectionFragment? = null

        fun getInstance(listener: RefreshListener): NoConnectionFragment {
            if (fragment == null) {
                fragment =
                    NoConnectionFragment(listener)
            }
            return fragment as NoConnectionFragment
        }
    }
}
