package com.spadesoft.gganalitycs.model

import android.util.Log
import com.spadesoft.gganalitycs.callbacks.OnCompleteDataRetrieve
import com.spadesoft.gganalitycs.firebase.getArticlesList
import com.spadesoft.gganalitycs.pojo.Article
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class ArticleModel {
    fun getArticles(onCompleteDataRetrieve: OnCompleteDataRetrieve) {
        val articlesLinks = ArrayList<String>()
        getArticlesList().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<String> {
                override fun onComplete() {
                    val articles = ArrayList<Article>()
                    com.spadesoft.gganalitycs.firebase.getArticles(articlesLinks).subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread()).subscribe(object : Observer<Article> {
                            override fun onComplete() {
                                onCompleteDataRetrieve.retrieve(articles)
                            }

                            override fun onSubscribe(d: Disposable) {}

                            override fun onNext(t: Article) {
                                articles.add(t)
                            }

                            override fun onError(e: Throwable) {
                                Log.i("populate list", e.message, e)
                            }
                        })
                }

                override fun onSubscribe(d: Disposable) {}

                override fun onNext(t: String) {
                    articlesLinks.add(t)
                }

                override fun onError(e: Throwable) {
                    Log.i("getArticles", e.message, e)
                }
            })
    }
}