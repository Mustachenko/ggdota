package com.spadesoft.gganalitycs

import android.app.Application
import com.spadesoft.gganalitycs.di.components.AppComponent
import com.spadesoft.gganalitycs.di.components.DaggerAppComponent
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth

class App : Application() {
    override fun onCreate() {
        super.onCreate()
        FirebaseApp.initializeApp(this)
        FirebaseAuth.getInstance().signInAnonymously()
        val component = DaggerAppComponent
            .builder().withApplication(this)
            .build()
        component.inject(this)
        appComponent = component
    }


    companion object {
        lateinit var appComponent: AppComponent
    }
}