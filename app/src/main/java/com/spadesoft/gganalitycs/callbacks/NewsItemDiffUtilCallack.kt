package com.spadesoft.gganalitycs.callbacks

import androidx.recyclerview.widget.DiffUtil
import com.spadesoft.gganalitycs.pojo.Article

class NewsItemDiffUtilCallack(val oldList: List<Article>, val newList: List<Article>) : DiffUtil.Callback() {
    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].pubTime == newList[newItemPosition].pubTime && oldList[oldItemPosition].content == newList[newItemPosition].content
    }

    override fun getOldListSize(): Int {
        return oldList.size
    }

    override fun getNewListSize(): Int {
        return newList.size
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].pubTime == newList[newItemPosition].pubTime
                && oldList[oldItemPosition].headerImage == newList[newItemPosition].headerImage
                && oldList[oldItemPosition].content == newList[newItemPosition].content
                && oldList[oldItemPosition].headerText == newList[newItemPosition].headerText
    }

}