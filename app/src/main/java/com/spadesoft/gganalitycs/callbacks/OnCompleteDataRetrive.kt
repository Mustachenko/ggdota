package com.spadesoft.gganalitycs.callbacks

interface OnCompleteDataRetrieve {
    fun retrieve(items: List<Any>)
}
